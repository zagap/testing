const connect = require('connect');
const connectSlow = require('connect-slow');
const serveStatic = require('serve-static');
const serveIndex = require('serve-index');
const open = require('open');

const PORT = 8080;
const PUBLIC_ROOT = './public';

const staticMw = serveStatic(PUBLIC_ROOT, {
	index: false
});

const indexMw = serveIndex(PUBLIC_ROOT, {
	filter: (filename) => {
		return /\.html$/i.test(filename);
	}
});

const slowMw = connectSlow({
	url: /slow\-files\//i,
	delay: 2000
});

connect()
	.use(slowMw)
	.use(staticMw)
	.use(indexMw)
	.listen(PORT);

const publicUrl = 'http://localhost:' + PORT;
open(publicUrl);

console.log('Running on ' + publicUrl);
