/*
    Author: Alexandr Zahatski
    Email: zahatski@gmail.com
    Best regards!
*/

var _cache_urls = new Map();

/**
 * Async fetch url and set result.
 *
 * @param url - url of source
 * @param setRes  - function(text) - callback for set text
 */
function get_async(url, setRes ) {
 
        //get from cache
        if(_cache_urls[url]) {
            setRes(_cache_urls[url]);
        }

	var r = new XMLHttpRequest();
        r.open('GET', url, true);
        r.onreadystatechange = function() {
            if(r.readyState !== 4) return;
            if(r.status === 200) {
                 var response = _cache_urls[url] = r.responseText;
                 var headerType = r.getResponseHeader('Content-Type') || '';
                 var cntType = headerType.split(';')[0];
                 var result = '';
                 switch ( cntType ) {
                    case 'application/javascript':
                        try {
                        response = 'var cb = function(d){return d.text}; return ' + response;
                         result = (new Function(response))();
                        }
                        catch(e) {
                            setRes('Error: Fail execute ' + url)
                        }
                        break;
                    case 'application/json':
                        result = JSON.parse(response).text;
                        break;
                    case 'text/plain':
                        result = response;
                        break;
                    default :
                         console.log('Not supported type: ' + cntType);
                         result = ""
                 }
                _cache_urls[url] = result; //Save in cache
                 setRes(result);
            }
            else {
                setRes(r.statusText); //if error
            }
        };

        r.send();
}

/**
 * Manipulate with two DOM's
 *
 * @param input - input element
 * @param output  - textarea for load to
 */

function sync(input,output) {
        //set "Загрузка..."
        output.value = 'Загрузка...';
        get_async(input.value, function(text) { output.value = text} );
}

function main() {

     var input = document.getElementById('input');
     var output = document.getElementById('output');
    
     input.onchange = function (event) {
	sync(input,output);
        };
      sync(input,output);
}

// run when both DOM is ready & page content is loaded
if (['complete', 'loaded', 'interactive'].includes(document.readyState) && document.body) {  
        main();
  } else {
       document.addEventListener('DOMContentLoaded', main, false);
  }

